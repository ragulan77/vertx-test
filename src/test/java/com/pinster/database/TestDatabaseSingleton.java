package com.pinster.database;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.kernel.GraphDatabaseAPI;
import org.neo4j.server.WrappingNeoServerBootstrapper;
import org.neo4j.server.configuration.Configurator;
import org.neo4j.server.configuration.ServerConfigurator;

public class TestDatabaseSingleton {
    private GraphDatabaseService graphDbService;
    private WrappingNeoServerBootstrapper bootstrapper;
    
    // testing database port to use 
    private final int PORT = 7777;
    
    // private static class for holding the Singleton
    private static class TestDatabaseSingletonHolder
    {
        private final static TestDatabaseSingleton instance = new TestDatabaseSingleton();
    }
    
    //default private constructor
    private TestDatabaseSingleton(){
        graphDbService = new org.neo4j.test.ImpermanentGraphDatabase();
        ServerConfigurator configurator = new ServerConfigurator((GraphDatabaseAPI) graphDbService);
        configurator.configuration().addProperty(Configurator.WEBSERVER_PORT_PROPERTY_KEY, PORT); 
        configurator.configuration().addProperty("dbms.security.auth_enabled", false); 
        bootstrapper = new WrappingNeoServerBootstrapper((GraphDatabaseAPI) graphDbService, configurator); 
    }
    
    public GraphDatabaseService getDatabaseService()
    {
        return graphDbService;
    }
    
    public int getPort()
    {
        return PORT;
    }
    
    //static method for getting the singleton
    public static TestDatabaseSingleton getInstance()
    {
        return TestDatabaseSingletonHolder.instance;
    }
    
    public void startDatabase()
    {
        bootstrapper.start();
    }
    
    public void stopDatabase()
    {
        bootstrapper.stop();
    }
    
    public boolean isRunning()
    {
        return bootstrapper.getServer().getDatabase().isRunning();
    }
}
