package com.pinster.database;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.kernel.GraphDatabaseAPI;
import org.neo4j.server.WrappingNeoServerBootstrapper;
import org.neo4j.server.configuration.Configurator;
import org.neo4j.server.configuration.ServerConfigurator;
import org.neo4j.test.TestGraphDatabaseFactory;

@RunWith(VertxUnitRunner.class)
public class NodeCreationVerticleTest {
    
    private Vertx vertx;
    private TestDatabaseSingleton databaseService = TestDatabaseSingleton.getInstance();

    @Before
    public void setUp(TestContext context) {
        databaseService.startDatabase();
        while (!databaseService.isRunning()) 
        { 
            try {
                System.out.println("waiting for neo4j server ...");
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } 
        } 
        System.out.println("neo4j server started !");
        
        vertx = Vertx.vertx();
        DeploymentOptions options = new DeploymentOptions().setWorker(true);
        SecureWebResource.setAuth("neo4j", "admin");
        SecureWebResource.setDomain("localhost");
        SecureWebResource.setPort(databaseService.getPort());
        vertx.deployVerticle(NodeCreationVerticle.class.getName(), options, context.asyncAssertSuccess());
    }
    
    @After
    public void tearDown(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
        databaseService.stopDatabase();
    }
    
    @Test
    public void testNodeCreation(TestContext context) {
        final Async async = context.async();
        EventBus eb = vertx.eventBus();
        JsonObject jsonUser = new JsonObject();
        jsonUser.put("first_name", "Guihon");
        
        eb.send("database.node.creation", jsonUser, ack -> {
            if (ack.succeeded()) {
                JsonObject answer = new JsonObject(ack.result().body().toString());                
                context.assertTrue(answer.getInteger("http_status_code") == HttpStatus.SC_CREATED);
                async.complete();
            }
            else {
                context.fail("Creation failed.");
            }
        });
    }
    
    @Test
    public void testNodeCreation2(TestContext context) {
        final Async async = context.async();
        EventBus eb = vertx.eventBus();
        JsonObject jsonUser = new JsonObject();
        jsonUser.put("first_name", "Guihon");
        
        eb.send("database.node.creation", jsonUser, ack -> {
            if (ack.succeeded()) {
                JsonObject answer = new JsonObject(ack.result().body().toString());                
                context.assertTrue(answer.getInteger("http_status_code") == HttpStatus.SC_CREATED);
                async.complete();
            }
            else {
                context.fail("Creation failed.");
            }
        });
    }

}
