package com.pinster.database;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(VertxUnitRunner.class)
public class QuerySenderVerticleTest {
    
    private Vertx vertx;

    @Before
    public void setUp(TestContext context) {
        vertx = Vertx.vertx();
        DeploymentOptions options = new DeploymentOptions().setWorker(true);
        SecureWebResource.setAuth("neo4j", "admin");
        SecureWebResource.setDomain("localhost");
        SecureWebResource.setPort(7474);
        vertx.deployVerticle(QuerySenderVerticle.class.getName(), options, context.asyncAssertSuccess());
    }
    
    @After
    public void tearDown(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }
    
    @Test
    public void testQuerySender(TestContext context) {
        final Async async = context.async();
        EventBus eb = vertx.eventBus();
        JsonObject rqst = new JsonObject("{\"statements\" : [ {\"statement\" : \"CREATE (n {last_name:'Mendy'}) RETURN id(n)\"} ]}");
        
        DeliveryOptions options =  new DeliveryOptions().addHeader("http_method", "POST")
                                                        .addHeader("relative_uri", "transaction/commit");
        eb.send("database.query.sender", rqst, options, ack -> {
            if (ack.succeeded()) {
                JsonObject answer = new JsonObject(ack.result().body().toString());                
                context.assertTrue(answer.getInteger("http_status_code") == HttpStatus.SC_OK);
                async.complete();
            }
            else {
                context.fail("Creation failed.");
            }
        });
    }

}
