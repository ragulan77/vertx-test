package com.pinster.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;


public class Transaction {
    List<String> statements; 
    //List<Map<String,JsonObject>> parameters; 
    List<Map<String,Object>> parameters; 
    
    public Transaction(){
        statements = new ArrayList<String>();
        parameters = new ArrayList<Map<String,Object>>();
    }
    
    /**
     * Inserts a statement into the Transaction to be created. Each statement will have an index start from 0 and a list of parameters.
     * If the index already exists, the new statement will replace the old one, and parameters will be reset.
     * 
     * @param index the index of statement
     * @param statement the statement to insert
     * @throws IndexOutOfBoundsException if the index is invalid 
     */
    public void addStatement(int index, String statement) throws IndexOutOfBoundsException
    {
        int size = statements.size();
        if(index < 0 || index > size)
            throw new IndexOutOfBoundsException("Invalid index " + index + ". Should be " + size);
        
        statements.add(index, statement);
        parameters.add(index, new HashMap<String,Object>());
    }
    
    
    
    /**
     * Add a parameter to a specific statement identified by its index. A statement can have several parameters.
     * IF the parameter already exists, it will be replaced by the new one.
     * 
     * @param statementIndex the index of statement for which you want to add a parameter
     * @param propertyName the property name which is inside the statement 
     * @param propertyValue the value you want to give 
     * @throws IndexOutOfBoundsException if the statement index is invalid
     */
    public void addParamToStatement(int statementIndex, String propertyName, Object propertyValue) throws IndexOutOfBoundsException
    {
        if(statementIndex < 0 || statementIndex >= statements.size())
            throw new IndexOutOfBoundsException("Statement " + statementIndex + " doesn't exist");
        
        Map<String,Object> parameter = parameters.get(statementIndex);
        parameter.put(propertyName, propertyValue);
    }
    
    /**
     * @return a JsonObject which represents the statements to send to Neo4j
     */
    public JsonObject getStatementsObject()
    {
        JsonObject jsonStatements = new JsonObject();
        jsonStatements.put("statements", new JsonArray());
        
        int nbStatements = statements.size();
        for(int i = 0; i < nbStatements; i++)
        {
            JsonObject fullStatement = new JsonObject();
            fullStatement.put("statement", statements.get(i));
            
            JsonObject allParameters = new JsonObject();
            Map<String,Object> parameter = parameters.get(i);
            for(Entry<String,Object> entry : parameter.entrySet())
            {
                allParameters.put(entry.getKey(), entry.getValue());
            }
            fullStatement.put("parameters", allParameters);
            
            jsonStatements.getJsonArray("statements").add(fullStatement);
        }
        
        return jsonStatements;
    }
}
