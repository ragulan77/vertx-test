package com.pinster.database;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class NodeCreationVerticle extends AbstractVerticle {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private HttpClientRequest request;
    private EventBus eb;

    @Override
    public void start() throws Exception {
        this.eb = getVertx().eventBus();
        logger.info("Correctly started");

        /* The transmitted message to this channel on the event bus must be a JsonObject */

        /* Node Creation */
        this.eb.consumer("database.node.creation", message -> createNode(message));

        /* Relationship Creation */
        /* Usually this is called after the creation ended successfully */
        this.eb.consumer("database.relationship.creation", message -> createRelationship(message));
    }
    
    private void createNode(Message<Object> message) {
        this.request = SecureWebResource.createHttpRequest(getVertx(), HttpMethod.POST, "node", response -> {
            Buffer totalBuffer = Buffer.buffer();
            response.handler(buffer -> {
                totalBuffer.appendBuffer(buffer);
            });

            handleResponse(message, response, totalBuffer);
        });

        this.request.end(((JsonObject)message.body()).encode(), "UTF-8");
    }
    
    private void createRelationship(Message<Object> message) {
        this.request = SecureWebResource.createHttpRequest(getVertx(), HttpMethod.POST, "node/" + message.headers().get("node_id") + "/relationship", response -> {
            Buffer totalBuffer = Buffer.buffer();
            response.handler(buffer -> {
                totalBuffer.appendBuffer(buffer);
            });

            handleResponse(message, response, totalBuffer);
        });

        this.request.end(((JsonObject)message.body()).encode(), "UTF-8");
    }

    private void handleResponse(Message<Object> message, HttpClientResponse response, Buffer totalBuffer) {
        response.endHandler(v -> {
            logger.info("Full body received, length = " + totalBuffer.length());
            logger.debug("Message received: " + response.headers().toString() + "\n" + totalBuffer.toString("UTF-8"));
            JsonObject json = new JsonObject(totalBuffer.toString("UTF-8"));
            json.put("http_status_code", response.statusCode());
            json.put("http_status_message", response.statusMessage());
            message.reply(json);
        });
    }
    
}
