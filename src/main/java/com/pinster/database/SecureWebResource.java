package com.pinster.database;

import java.util.Base64;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.http.HttpMethod;

public class SecureWebResource {

    private static String auth = "neo4j:neo4j";
    private static int port = 7474;
    private static String domain = "localhost";
    private final static String ROOT_URI = "/db/data/";

    public static HttpClientRequest createHttpRequest(Vertx vertx, HttpMethod method, String uri,
            Handler<HttpClientResponse> clientRespHandler) {
        String base64Auth = "Basic " + Base64.getEncoder().encodeToString(auth.getBytes());
        HttpClient client = vertx.createHttpClient();

        HttpClientRequest request = client.request(method, port, domain, ROOT_URI + uri, clientRespHandler);
        request.putHeader("Accept", "application/json; charset=UTF-8");
        request.putHeader("Authorization", base64Auth);
        request.putHeader("Content-Type", "application/json");
        return request;
    }

    public static String setAuth(String login, String pass) {
        return SecureWebResource.auth = login + ":" + pass;
    }

    public static int setPort(int port) {
        return SecureWebResource.port = port;
    }

    public static String setDomain(String domain) {
        return SecureWebResource.domain = domain;
    }

}
