package com.pinster.database;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;


public class GetNodeVerticle extends AbstractVerticle{
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private HttpClientRequest request;
    private EventBus eb;    
	
    @Override
    public void start() throws Exception {
        this.eb = getVertx().eventBus();
        logger.info("Correctly started");

        /* Getting node by ID */        
        this.eb.consumer("database.node.get.by.id", message -> getNodeById(message));
    }
    
    private void getNodeById(Message<Object> message) {
        this.request = SecureWebResource.createHttpRequest(getVertx(), HttpMethod.GET, "node/" + message.headers().get("node_id"), response -> {
            Buffer totalBuffer = Buffer.buffer();
            response.handler(buffer -> {
                totalBuffer.appendBuffer(buffer);
            });

            response.endHandler(v -> {
                logger.info("Full body received, length = " + totalBuffer.length());
                logger.debug("Message received: " + response.headers().toString() + "\n" + totalBuffer.toString("UTF-8"));
                JsonObject json = new JsonObject(totalBuffer.toString("UTF-8"));
                json.put("http_status_code", response.statusCode());
                json.put("http_status_message", response.statusMessage());
                message.reply(json);
            });
        });

        this.request.end(((JsonObject)message.body()).encode(), "UTF-8");
    }
}
