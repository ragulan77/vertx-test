package com.pinster.database;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class QuerySenderVerticle extends AbstractVerticle {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private HttpClientRequest request;
    private EventBus eb;

    @Override
    public void start() throws Exception {
        this.eb = getVertx().eventBus();
        logger.info("Correctly started");

        /* The transmitted message to this channel on the event bus must be a JsonObject */
        /* containing the statement(s) and the parameter(s)                              */
        this.eb.consumer("database.query.sender", message -> {
            String strMethod = message.headers().get("http_method");
            String relativeUri = message.headers().get("relative_uri"); // for example: "transaction/commit"
            HttpMethod method = QuerySenderVerticle.getHttpMethod(strMethod);

            this.request = SecureWebResource.createHttpRequest(getVertx(), method, relativeUri, response -> {
                Buffer totalBuffer = Buffer.buffer();
                response.handler(buffer -> {
                    totalBuffer.appendBuffer(buffer);
                });

                response.endHandler(v -> {
                    logger.info("Full body received, length = " + totalBuffer.length());
                    logger.debug("Message received: " + response.headers().toString() + "\n" + totalBuffer.toString("UTF-8"));
                    JsonObject json = new JsonObject(totalBuffer.toString("UTF-8"));
                    json.put("http_status_code", response.statusCode());
                    json.put("http_status_message", response.statusMessage());
                    message.reply(json);
                });
            });

            this.request.end(((JsonObject)message.body()).encode(), "UTF-8");
        });
    }

    private static HttpMethod getHttpMethod(String strMethod) {
        HttpMethod method;
        switch (strMethod) {
            case "POST":
                method = HttpMethod.POST;
                break;
            case "GET":
                method = HttpMethod.GET;
                break;
            default:
                method = null;
        }
        return method;
    }
}
