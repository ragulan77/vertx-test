/**
 * 
 */
package com.pinster;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpServer;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

/**
 * @author gmendy, rcharles
 *
 */
public class JobVerticle extends AbstractVerticle {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private HttpServer server;
    private EventBus eb;
    private Router router;
    
    @Override
    public void start() {
        eb = getVertx().eventBus();
        server = getVertx().createHttpServer();
        router = Router.router(getVertx());
        
        router.get("/jobs/:lat/:lon/:scale").handler(this::getJobs);
        
        logger.info("Correctly started");
    }
    
    private void getJobs(RoutingContext context) {
        // create a request with the coordinates
        // then query neo4j about what's in the
        // zone represented by lat/lon/scale
        // we'll update this with some other criterias later
    }
}
