package com.pinster.user;

import com.pinster.database.Transaction;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.web.RoutingContext;


public class User {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    @SuppressWarnings("unused")
    private JWTAuth authProvider;
    private EventBus eventBus;
    
    
    public User()
    {
    }

    
    public User(Vertx vertx)
    {
        // configuration of auth provider in order to generate token
        JsonObject config = new JsonObject().put("keyStore", new JsonObject()
                                            .put("path", "src/main/java/com/habitacio/user/keystore.jceks")
                                            .put("type", "jceks")
                                            .put("password", "secret"));
        
        authProvider = JWTAuth.create(vertx, config);
        eventBus = vertx.eventBus();
    }
    
    public void login(RoutingContext context)
    {
        try{
            String login = context.request().getParam("login");
            String pwd = context.request().getParam("password");

            DeliveryOptions options = new DeliveryOptions().addHeader("http_method", "POST")
                                                           .addHeader("relative_uri", "transaction/commit");
            
            Transaction tr = new Transaction();
            tr.addStatement(0, "MATCH (n { login: {login}, password:{password} }) RETURN n");
            tr.addParamToStatement(0, "login", login);
            tr.addParamToStatement(0, "password", pwd);
            

            JsonObject statements = tr.getStatementsObject();
            
            logger.info(statements.toString());
            eventBus.send("database.query.sender", statements, options, ar -> {
                if(ar.succeeded())
                {
                    logger.info("User info received");
                    context.response().putHeader("Content-Type", "text/plain");
                    context.response().end(ar.result().body().toString());
                }
                else {
                    logger.error("Error during node fetching");
                }
            });
        }
        catch(IndexOutOfBoundsException e)
        {
            logger.error(e.getMessage());
        }
    }
    
    public void create(RoutingContext context)
    {
        JsonObject user = context.get("user");

        Transaction tr = new Transaction();
        try {
            tr.addStatement(0, "CREATE (n:User {params}) RETURN n");
            tr.addParamToStatement(0, "params", user);
            JsonObject statements = tr.getStatementsObject();
            
            DeliveryOptions options = new DeliveryOptions().addHeader("http_method", "POST")
                                                           .addHeader("relative_uri", "transaction/commit");

            eventBus.send("database.query.sender", statements, options, ack -> {
                if (ack.succeeded()) {
                    logger.info(getClass().getName() + " Response received :");
                    logger.info(ack.result().body());
                }
                else {
                    logger.error(getClass().getName() + " Failed.");
                }
            });
        } catch (IndexOutOfBoundsException e) {
            logger.error(e.getMessage());
        }
    }

}
