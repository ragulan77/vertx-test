package com.pinster.user;

import org.apache.http.HttpStatus;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;

public class UserVerticle extends AbstractVerticle {
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private User userApi;
    private HttpServer server;
    private HttpClientRequest request;
    private EventBus eb;
    
    @Override
    public void start() throws Exception {
        eb = getVertx().eventBus();
        logger.info("Correctly started");
        userApi = new User(getVertx());        
        server = getVertx().createHttpServer();
        
        server.requestHandler(createRoute()::accept).listen(config().getInteger("http.port", 8080));
        
        /* Getting users by login*/
        this.eb.consumer("database.node.get.by.login", message -> getUserByLogin(message));
    }
    
    
    public void stop() throws Exception {
        server.close();
    }
    
    
    private Router createRoute()
    {
        Router router = Router.router(getVertx());
        router.get("/login").handler(userApi::login);
        router.get("/create").handler(userApi::create);
        
        return router;
    }
    
    
    private void getUserByLogin(Message<Object> message) {
        String login = ((JsonObject)message.body()).getString("login");
        JsonArray array = new JsonArray()
                                    .add(new JsonObject()
                                    .put("statement", "MATCH (n:User{login: '" + login + "'}) RETURN n")
                                    .put("resultDataContents", "[ \"REST\" ]"));
        JsonObject query = new JsonObject().put("statements", array);
        
        DeliveryOptions options = new DeliveryOptions().addHeader("http_method", "GET")
                                                       .addHeader("relative_uri", "transaction/commit");
        
        logger.info("Getting user by login: " + login);
        logger.debug("Query: " + query.encode());
        
        eb.send("database.query.sender", query, options, ack -> {
            if (ack.succeeded()) {
                JsonObject answer = new JsonObject(ack.result().body().toString());                
                
                if(answer.getInteger("http_status_code") == HttpStatus.SC_OK) {
                    logger.info("Query execution succeeded");
                }
                message.reply(answer);
            }
            else {
                logger.error("Query execution failed");
            }
        });
    }
    
}
