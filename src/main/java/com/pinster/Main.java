package com.pinster;

import com.pinster.database.SecureWebResource;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class Main {

    private final static Logger logger = LoggerFactory.getLogger("com.pinster.Main");

    public static void main(String[] args) {
        logger.info("Starting Vertx...");
        Vertx vertx = Vertx.vertx();
        
        JsonObject config = new JsonObject().put("http.port", 8080);
        
        DeploymentOptions options = new DeploymentOptions().setWorker(true);
        options.setConfig(config);
        
        SecureWebResource.setAuth("neo4j", "admin");
        SecureWebResource.setDomain("localhost");
        SecureWebResource.setPort(7474);

        vertx.deployVerticle("com.habitacio.database.GetNodeVerticle", options);
        vertx.deployVerticle("com.habitacio.user.UserVerticle", options);
        vertx.deployVerticle("com.habitacio.database.NodeCreationVerticle", options);
        vertx.deployVerticle("com.habitacio.database.QuerySenderVerticle", options);
    }

}
